	.data	
V1:     .space  64
M:      .word   0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1
        .word   0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0
        .word   1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0
        .word   0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0
        .word   1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0
        .word   0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1
        .word   0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1
        .word   0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0
        .word   0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0
        .word   0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1
        .word   0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1
        .word   1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0
        .word   0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0
        .word   1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0
        .word   0,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0
        .word   0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1
V2:     .word   -5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10

	.text
	.globl main
	# k = $t0, i = $t1, j = $t2
	# tmp = $t3
	# $t4 = 4
	# $t5 = 16
main:
	addiu 	$sp,$sp,-12
	sw 	$s0, 0($sp)
	sw 	$s1, 4($sp)
	sw 	$s2, 8($sp)
	
        la      $s0, M       	# $s0 = @M[0][0]
        la      $s1, V1         # $s1 = @V1[0]
        la      $s2, V2		# $s2 = @V2[0]
        li 	$t4,4
        li 	$t5,16
        li      $t0, 0          # $t0 = k = 0
for1:
        bge     $t0, $t4, end_for1
        li      $t1, 0          # $t1 = i = 0
for2:
	bge     $t1, $t5, end_for2
	sll 	$t6,$t1,6	# i*64
	sll 	$t7,$t0,4	# k*16
	addu 	$t6,$s0,$t6
	addu	$t6,$t6,$t7	# &M[i][4*k]
	
	addu 	$t7,$s2,$t7	# &V2[4*k]
	li      $t3, 0          # $t3 = tmp
	li      $t2, 0          # $t2 = j = 0
for3:	
	bge     $t2, $t4, end_for3
	
        lw      $a0, 0($t6)
        lw      $a1, 0($t7)
        mult    $a0, $a1
        mflo    $a0
        addu    $t3, $t3, $a0
        
        addiu   $t6, $t6, 4
        addiu   $t7, $t7, 4
        addiu   $t2, $t2, 1
        b       for3
end_for3:
	sll 	$t6,$t1,2	# i*4
	addu 	$t6,$t6,$s1	# &V1[i]
        lw      $t7, 0($t6)
        addu   	$t7, $t7, $t3
        sw 	$t7, 0($t6)
        addiu   $t1, $t1, 1
        b       for2
end_for2:
	
	addiu $t0, $t0, 1
	b for1
end_for1:
	lw 	$s0, 0($sp)
	lw 	$s1, 4($sp)
	lw 	$s2, 8($sp)
	addiu 	$sp,$sp,12
	jr	$ra

