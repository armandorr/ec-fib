# PRACTICA 1 #######################

	.data
fib: .word 0,0,0,0,0,0,0,0,0,0

	.text
	.globl main
main:
	li $s0,2
	la $s1,fib
	sw $zero,0($s1)
	li $t0,1
	sw $t0,4($s1)
while:
	slti $t0,$s0,10
	beq $t0,$zero, fi
	
	sll $t0,$s0,2
	addu $t0,$t0,$s1
	lw $t1,-4($t0)
	lw $t2,-8($t0)
	addu $t1,$t1,$t2
	sw $t1,0($t0)
	
	addiu $s0,$s0,1
	b while
fi:
	jr $ra		# main retorna al codi de startup
