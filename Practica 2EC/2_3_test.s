	.data
w:        .asciiz  "8754830094826456674949263746929"
resultat: .byte 0

	.text
	.globl main
main:
	addiu	$sp, $sp, -4
	sw	$ra, 0($sp)
	la	$a0, w
	li	$a1, 31
	jal	moda
	la	$s0, resultat
	sb	$v0, 0($s0)
	move	$a0, $v0
	li	$v0, 11
	syscall
	lw	$ra, 0($sp)
	addiu	$sp, $sp, 4
	jr 	$ra

nofares:
	li	$t0, 0x12345678
	move	$t1, $t0
	move	$t2, $t0
	move	$t3, $t0
	move	$t4, $t0
	move 	$t5, $t0
	move	$t6, $t0
	move 	$t7, $t0
	move 	$t8, $t0
	move 	$t9, $t0
	move	$a0, $t0
	move	$a1, $t0
	move	$a2, $t0
	move	$a3, $t0
	jr	$ra


moda:
# ESCRIU AQUI EL TEU CODI
	addiu $sp,$sp,-56 
	sw $s0,40($sp)
	sw $s1,44($sp)
	sw $s2,48($sp)
	sw $ra,52($sp)
	
	li $t0,0   # k = 0
	li $t1,10
	move $t2,$sp
	
for1: 	bge $t0,$t1,fin_for1 
	sw $zero,0($t2)		# histo[k] = 0
	addiu $t2,$t2,4		# $sp += 4
	addiu $t0,$t0,1		#++k
	b for1
	
fin_for1:
	li $s1,0 # k
	li $t0,'0' # max
	move $s2,$a0 # vec
	move $s0,$a1 # num
	
for2:
	bge $s1,$s0,fin_for2
	move $a0,$sp		# a0 = histo
	
	addu $t1,$s2,$s1	# &vec[k]
	lb $t1,0($t1)		# vec[k]
	addiu $a1,$t1,-48	# a1 = vec[k] - '0'
	
	addiu $a2,$t0,-48	# a2 = max - '0'
	
	jal update
	
	addiu $t0,$v0,'0'       # max = '0' + update	
	addiu $s1,$s1,1		# ++k
	b for2
					
fin_for2:
	addiu $v0,$v0,'0'
	lw $s0,40($sp)
	lw $s1,44($sp)
	lw $s2,48($sp)
	lw $ra,52($sp)	
	addiu $sp,$sp,56
	jr $ra

update:
# ESCRIU AQUI EL TEU CODI
	addiu $sp,$sp,-16
	sw $s0,0($sp)
	sw $s1,4($sp)
	sw $s2,8($sp)
	sw $ra,12($sp)
	
	move $s0,$a0
	move $s1,$a1
	move $s2,$a2
	
	jal nofares
	
	sll $t0,$s1,2
	addu $t0,$s0,$t0
	lw $t1,0($t0)
	addiu $t1,$t1,1
	sw $t1,0($t0)
	
	sll $t2,$s2,2
	addu $t3,$t2,$s0
	lw $t0,0($t3)
	ble $t1,$t0,else
	move $v0,$s1
	b fi
	
else:	move $v0,$s2

fi:
	lw $s0,0($sp)
	lw $s1,4($sp)
	lw $s2,8($sp)
	lw $ra,12($sp)
	addiu $sp,$sp,16
	
	jr $ra

