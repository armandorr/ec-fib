# PRACTICA 2 #######################

	.data
	
result: .word 0
num:	.byte ';'

	.text
	
	.globl main
main:
	la $t0,num
	lw $t0,0($t0)
	la $t1,result
	
a:	li $t2,'a'
	blt $t0,$t2,A
	
z:	li $t2,'z'
	ble $t0,$t2,if

A:	li $t2,'A'
	blt $t0,$t2,else

Z:	li $t2,'z'
	bgt $t0,$t2,else

if:	sw $t0,0($t1)
	b fi

else:	li $t2,'0'
	blt $t0,$t2,else2
	
	li $t3,'9'
	bgt $t0,$t3,else2
	
	subu $t2,$t0,$t2
	sw $t2,0($t1)
	b fi
	
else2:	li $t2,-1
	sw $t2,0($t1)

fi:	jr $ra		# main retorna al codi de startup

