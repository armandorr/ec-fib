# PRACTICA 2 #######################

	.data
x: 	.word 31
y:	.word 0x00aa

	.text
	.globl main
main:
	la $t3,x
	lw $s0,0($t3)
	lw $s1,4($t3)
	li $t0,1
	sllv $t1,$t0,$s0
	subu $t1,$t1,$t0
	xor $s1,$s1,$t1

	jr $ra		# main retorna al codi de startup

