	# Sessio 3

	.data 
# Declara aqui les variables mat1, mat4 i col
mat1: 	.space 120
mat4: 	.word 2,3,1,2,4,3
col:	.word 2

	.text 
	.globl main
main:
# Escriu aqui el programa principal
	addiu $sp,$sp,-4
	sw $ra,0($sp)
	
	la $a0,mat4		
	addiu $a1,$a0,8		#a1 = &mat4[0][2]
	lw $a1,0($a1)		#a1 = mat[0][2]
	la $a2,col
	lw $a2,0($a2)		#a2 = col=2
	
	jal subr
	
	la $t0,mat1
	sw $v0,108($t0)
	
	la $a0,mat4
	li $a1,1
	li $a2,1
	
	jal subr
	
	la $t0,mat1
	sw $v0,0($t0)
	
	lw $ra,0($sp)
	addiu $sp,$sp,4
	
	jr $ra
	
subr:
# Escriu aqui el codi de la subrutina
	sll $t0,$a1,1
	addu $t0,$t0,$a1	#3*i
	
	addu $t0,$t0,$a2	#3*i + j
	sll $t0,$t0,2		#(3*i + j)*4
	addu $t0,$t0,$a0	#x + (3*i + j)*4 = &x[i][j]
	lw $t0,0($t0)		#x[i][j]
	
	la $t1,mat1
	li $t2,24		
	mult $a2,$t2
	mflo $t2
	addu $t1,$t1,$t2	#mat1 + i*NC*T
	sw $t0,20($t1)		#mat1[j][5] = x[i][j]
	
	move $v0,$a1
	
	jr $ra
	

